################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ButtonGlue.cpp \
../src/EncoderGlue.cpp \
../src/LedGlue.cpp \
../src/MotorGlue.cpp \
../src/Timer.cpp 

OBJS += \
./src/ButtonGlue.o \
./src/EncoderGlue.o \
./src/LedGlue.o \
./src/MotorGlue.o \
./src/Timer.o 

CPP_DEPS += \
./src/ButtonGlue.d \
./src/EncoderGlue.d \
./src/LedGlue.d \
./src/MotorGlue.d \
./src/Timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ --std=c++14 -I../runtime -I../generated -I../src -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


