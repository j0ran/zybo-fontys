################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../generated/button.cc \
../generated/buttonled.cc \
../generated/ledtest.cc \
../generated/motortest.cc \
../generated/zybo.cc 

CC_DEPS += \
./generated/button.d \
./generated/buttonled.d \
./generated/ledtest.d \
./generated/motortest.d \
./generated/zybo.d 

OBJS += \
./generated/button.o \
./generated/buttonled.o \
./generated/ledtest.o \
./generated/motortest.o \
./generated/zybo.o 


# Each subdirectory must supply rules for building sources it contributes
generated/%.o: ../generated/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ --std=c++14 -I../runtime -I../generated -I../src -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


