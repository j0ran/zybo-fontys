################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../runtime/pump.cc \
../runtime/runtime.cc 

CC_DEPS += \
./runtime/pump.d \
./runtime/runtime.d 

OBJS += \
./runtime/pump.o \
./runtime/runtime.o 


# Each subdirectory must supply rules for building sources it contributes
runtime/%.o: ../runtime/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ --std=c++14 -I../runtime -I../generated -I../src -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


