#include <iostream>
#include <thread>

#include "TimerID.h"
#include "LedGlue.hh"
#include "ButtonGlue.hh"
#include "ledtest.hh"


int main()
{
	std::cout << "Zybo Example Program Dezyne ButtonLED" << std::endl;

	dzn::locator locator;

	dzn::pump pump;
	locator.set(pump);

	dzn::runtime runtime;
	locator.set(runtime);

	TimerID timerID;
	locator.set(timerID);

	LedTestSystem ledTestSystem(locator);
	LedGlue ledGlue(locator, "/sys/class/gpio/gpio964");
	ButtonGlue buttonGlue(locator, "/sys/class/gpio/gpio960");
	connect(ledGlue.api, ledTestSystem.ledGlue);
	connect(buttonGlue.api, ledTestSystem.buttonGlue);
	ledTestSystem.check_bindings();

	dzn::shell(pump, ledTestSystem.api.in.start);
	std::this_thread::sleep_for(std::chrono::hours(1));
	dzn::shell(pump, ledTestSystem.api.in.stop);
}
