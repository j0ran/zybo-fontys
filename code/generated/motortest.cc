#include "motortest.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>



TestMotor::TestMotor(const dzn::locator& dzn_locator)
: dzn_meta{"","TestMotor",0,0,{& buttonDutyAdd.meta,& buttonDutySub.meta,& switchDirection.meta,& switchEnabled.meta,& motor.meta,& periodTimer.meta,& dutyTimer.meta},{},{[this]{api.check_bindings();},[this]{buttonDutyAdd.check_bindings();},[this]{buttonDutySub.check_bindings();},[this]{switchDirection.check_bindings();},[this]{switchEnabled.check_bindings();},[this]{motor.check_bindings();},[this]{periodTimer.check_bindings();},[this]{dutyTimer.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)
, state(::TestMotor::State::Uninitialized), dutyState(::TestMotor::ValueState::Idle), duty(1000), step(500), directionState(::TestMotor::DirectionState::StoppedCW)

, api({{"api",this,&dzn_meta},{"",0,0}})

, buttonDutyAdd({{"",0,0},{"buttonDutyAdd",this,&dzn_meta}})
, buttonDutySub({{"",0,0},{"buttonDutySub",this,&dzn_meta}})
, switchDirection({{"",0,0},{"switchDirection",this,&dzn_meta}})
, switchEnabled({{"",0,0},{"switchEnabled",this,&dzn_meta}})
, motor({{"",0,0},{"motor",this,&dzn_meta}})
, periodTimer({{"",0,0},{"periodTimer",this,&dzn_meta}})
, dutyTimer({{"",0,0},{"dutyTimer",this,&dzn_meta}})


{
  dzn_rt.performs_flush(this) = true;

  api.in.initialize = [&](){return dzn::call_in(this,[=]{ return api_initialize();}, this->api.meta, "initialize");};
  api.in.terminate = [&](){return dzn::call_in(this,[=]{ return api_terminate();}, this->api.meta, "terminate");};
  api.in.start = [&](){return dzn::call_in(this,[=]{ return api_start();}, this->api.meta, "start");};
  api.in.stop = [&](){return dzn::call_in(this,[=]{ return api_stop();}, this->api.meta, "stop");};
  buttonDutyAdd.out.turnedOn = [&](){return dzn::call_out(this,[=]{ return buttonDutyAdd_turnedOn();}, this->buttonDutyAdd.meta, "turnedOn");};
  buttonDutyAdd.out.turnedOff = [&](){return dzn::call_out(this,[=]{ return buttonDutyAdd_turnedOff();}, this->buttonDutyAdd.meta, "turnedOff");};
  buttonDutySub.out.turnedOn = [&](){return dzn::call_out(this,[=]{ return buttonDutySub_turnedOn();}, this->buttonDutySub.meta, "turnedOn");};
  buttonDutySub.out.turnedOff = [&](){return dzn::call_out(this,[=]{ return buttonDutySub_turnedOff();}, this->buttonDutySub.meta, "turnedOff");};
  switchDirection.out.turnedOn = [&](){return dzn::call_out(this,[=]{ return switchDirection_turnedOn();}, this->switchDirection.meta, "turnedOn");};
  switchDirection.out.turnedOff = [&](){return dzn::call_out(this,[=]{ return switchDirection_turnedOff();}, this->switchDirection.meta, "turnedOff");};
  switchEnabled.out.turnedOn = [&](){return dzn::call_out(this,[=]{ return switchEnabled_turnedOn();}, this->switchEnabled.meta, "turnedOn");};
  switchEnabled.out.turnedOff = [&](){return dzn::call_out(this,[=]{ return switchEnabled_turnedOff();}, this->switchEnabled.meta, "turnedOff");};
  periodTimer.out.timeout = [&](){return dzn::call_out(this,[=]{ return periodTimer_timeout();}, this->periodTimer.meta, "timeout");};
  dutyTimer.out.timeout = [&](){return dzn::call_out(this,[=]{ return dutyTimer_timeout();}, this->dutyTimer.meta, "timeout");};





}

void TestMotor::api_initialize()
{
  if ((state == ::TestMotor::State::Uninitialized && true)) 
  {
    this->motor.in.stop();
    this->motor.in.period(10000);
    duty = 1000;
    this->motor.in.duty(duty);
    state = ::TestMotor::State::Idle;
  }
  else if ((!((state == ::TestMotor::State::Uninitialized && true)) && true)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::api_terminate()
{
  if ((state == ::TestMotor::State::Uninitialized && true)) ;
  else if ((state == ::TestMotor::State::Idle && true)) 
  {
    state = ::TestMotor::State::Uninitialized;
  }
  else if ((!((state == ::TestMotor::State::Idle && true)) && (!((state == ::TestMotor::State::Uninitialized && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::api_start()
{
  if ((state == ::TestMotor::State::Idle && true)) 
  {
    this->motor.in.duty(duty);
    this->switchDirection.in.start();
    dutyState = ::TestMotor::ValueState::Idle;
    state = ::TestMotor::State::Starting1;
  }
  else if ((!((state == ::TestMotor::State::Idle && true)) && true)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::api_stop()
{
  if ((state == ::TestMotor::State::Idle && true)) ;
  else if ((state == ::TestMotor::State::Starting1 && true)) 
  {
    this->switchDirection.in.stop();
    state = ::TestMotor::State::Idle;
  }
  else if ((state == ::TestMotor::State::Starting2 && true)) 
  {
    this->switchDirection.in.stop();
    this->switchEnabled.in.stop();
    state = ::TestMotor::State::Idle;
  }
  else if ((state == ::TestMotor::State::Operational && true)) 
  {
    this->buttonDutyAdd.in.stop();
    this->buttonDutySub.in.stop();
    this->switchDirection.in.stop();
    this->switchEnabled.in.stop();
    this->motor.in.stop();
    this->periodTimer.in.cancel();
    this->dutyTimer.in.cancel();
    state = ::TestMotor::State::Idle;
  }
  else if ((!((state == ::TestMotor::State::Operational && true)) && (!((state == ::TestMotor::State::Starting2 && true)) && (!((state == ::TestMotor::State::Starting1 && true)) && (!((state == ::TestMotor::State::Idle && true)) && true))))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::buttonDutyAdd_turnedOn()
{
  if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) 
  {
    duty = duty + step;
    this->motor.in.duty(duty);
    this->dutyTimer.in.create(1000);
    dutyState = ::TestMotor::ValueState::Increasing;
  }
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) ;
  else if ((!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::buttonDutyAdd_turnedOff()
{
  if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) ;
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) 
  {
    this->dutyTimer.in.cancel();
    dutyState = ::TestMotor::ValueState::Idle;
  }
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) ;
  else if ((!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) && true)))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::buttonDutySub_turnedOn()
{
  if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) 
  {
    duty = duty - step;
    this->motor.in.duty(duty);
    this->dutyTimer.in.create(1000);
    dutyState = ::TestMotor::ValueState::Decreasing;
  }
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) ;
  else if ((!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::buttonDutySub_turnedOff()
{
  if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) ;
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) ;
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) 
  {
    this->dutyTimer.in.cancel();
    dutyState = ::TestMotor::ValueState::Idle;
  }
  else if ((!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Idle) && true)) && true)))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::switchDirection_turnedOn()
{
  if ((state == ::TestMotor::State::Starting1 && true)) 
  {
    directionState = ::TestMotor::DirectionState::CW;
    this->switchEnabled.in.start();
    state = ::TestMotor::State::Starting2;
  }
  else if ((state == ::TestMotor::State::Starting2 && true)) 
  {
    directionState = ::TestMotor::DirectionState::CW;
  }
  else if ((state == ::TestMotor::State::Operational && true)) 
  {
    {
      if (directionState == ::TestMotor::DirectionState::CCW) 
      {
        directionState = ::TestMotor::DirectionState::CW;
        this->motor.in.cw();
      }
    }
    {
      if (directionState == ::TestMotor::DirectionState::StoppedCCW) directionState = ::TestMotor::DirectionState::StoppedCW;
    }
  }
  else if ((!((state == ::TestMotor::State::Operational && true)) && (!((state == ::TestMotor::State::Starting2 && true)) && (!((state == ::TestMotor::State::Starting1 && true)) && true)))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::switchDirection_turnedOff()
{
  if ((state == ::TestMotor::State::Starting1 && true)) 
  {
    directionState = ::TestMotor::DirectionState::CCW;
    this->switchEnabled.in.start();
    state = ::TestMotor::State::Starting2;
  }
  else if ((state == ::TestMotor::State::Starting2 && true)) 
  {
    directionState = ::TestMotor::DirectionState::CCW;
  }
  else if ((state == ::TestMotor::State::Operational && true)) 
  {
    {
      if (directionState == ::TestMotor::DirectionState::CW) 
      {
        directionState = ::TestMotor::DirectionState::CCW;
        this->motor.in.ccw();
      }
    }
    {
      if (directionState == ::TestMotor::DirectionState::StoppedCW) directionState = ::TestMotor::DirectionState::StoppedCCW;
    }
  }
  else if ((!((state == ::TestMotor::State::Operational && true)) && (!((state == ::TestMotor::State::Starting2 && true)) && (!((state == ::TestMotor::State::Starting1 && true)) && true)))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::switchEnabled_turnedOn()
{
  if ((state == ::TestMotor::State::Starting2 && true)) 
  {
    initialize();
    state = ::TestMotor::State::Operational;
  }
  else if ((state == ::TestMotor::State::Operational && true)) 
  {
    {
      if (directionState == ::TestMotor::DirectionState::StoppedCW) 
      {
        this->motor.in.cw();
        directionState = ::TestMotor::DirectionState::CW;
      }
    }
    {
      if (directionState == ::TestMotor::DirectionState::StoppedCCW) 
      {
        this->motor.in.ccw();
        directionState = ::TestMotor::DirectionState::CCW;
      }
    }
  }
  else if ((!((state == ::TestMotor::State::Operational && true)) && (!((state == ::TestMotor::State::Starting2 && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::switchEnabled_turnedOff()
{
  if ((state == ::TestMotor::State::Starting2 && true)) 
  {
    {
      if (directionState == ::TestMotor::DirectionState::CW) directionState = ::TestMotor::DirectionState::StoppedCW;
    }
    {
      if (directionState == ::TestMotor::DirectionState::CCW) directionState = ::TestMotor::DirectionState::StoppedCCW;
    }
    initialize();
    state = ::TestMotor::State::Operational;
  }
  else if ((state == ::TestMotor::State::Operational && true)) 
  {
    {
      if (directionState == ::TestMotor::DirectionState::CW) directionState = ::TestMotor::DirectionState::StoppedCW;
    }
    {
      if (directionState == ::TestMotor::DirectionState::CCW) directionState = ::TestMotor::DirectionState::StoppedCCW;
    }
    this->motor.in.stop();
  }
  else if ((!((state == ::TestMotor::State::Operational && true)) && (!((state == ::TestMotor::State::Starting2 && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::periodTimer_timeout()
{
  if (true) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void TestMotor::dutyTimer_timeout()
{
  if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) 
  {
    duty = duty + step;
    this->motor.in.duty(duty);
    this->dutyTimer.in.create(500);
  }
  else if (((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) 
  {
    duty = duty - step;
    this->motor.in.duty(duty);
    this->dutyTimer.in.create(500);
  }
  else if ((!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Decreasing) && true)) && (!(((state == ::TestMotor::State::Operational && dutyState == ::TestMotor::ValueState::Increasing) && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}

void TestMotor::initialize () 
{
  {
    if (directionState == ::TestMotor::DirectionState::CW) this->motor.in.cw();
  }
  {
    if (directionState == ::TestMotor::DirectionState::CCW) this->motor.in.ccw();
  }
  this->buttonDutyAdd.in.start();
  this->buttonDutySub.in.start();
  return ;
}

void TestMotor::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void TestMotor::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}

//SYSTEM

TestMotorSystem::TestMotorSystem(const dzn::locator& dzn_locator)
: dzn_meta{"","TestMotorSystem",0,0,{& buttonDutyAdd.meta,& buttonDutySub.meta,& switchDirection.meta,& switchEnabled.meta,& motor.meta},{& impl.dzn_meta,& periodTimer.dzn_meta,& dutyTimer.dzn_meta,& dutyAdd.dzn_meta,& dutySub.dzn_meta,& direction.dzn_meta,& enabled.dzn_meta},{[this]{api.check_bindings();},[this]{buttonDutyAdd.check_bindings();},[this]{buttonDutySub.check_bindings();},[this]{switchDirection.check_bindings();},[this]{switchEnabled.check_bindings();},[this]{motor.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)


, impl(dzn_locator)
, periodTimer(dzn_locator)
, dutyTimer(dzn_locator)
, dutyAdd(dzn_locator)
, dutySub(dzn_locator)
, direction(dzn_locator)
, enabled(dzn_locator)

, api(impl.api)
, buttonDutyAdd(dutyAdd.glue), buttonDutySub(dutySub.glue), switchDirection(direction.glue), switchEnabled(enabled.glue), motor(impl.motor)
{


  impl.dzn_meta.parent = &dzn_meta;
  impl.dzn_meta.name = "impl";
  periodTimer.dzn_meta.parent = &dzn_meta;
  periodTimer.dzn_meta.name = "periodTimer";
  dutyTimer.dzn_meta.parent = &dzn_meta;
  dutyTimer.dzn_meta.name = "dutyTimer";
  dutyAdd.dzn_meta.parent = &dzn_meta;
  dutyAdd.dzn_meta.name = "dutyAdd";
  dutySub.dzn_meta.parent = &dzn_meta;
  dutySub.dzn_meta.name = "dutySub";
  direction.dzn_meta.parent = &dzn_meta;
  direction.dzn_meta.name = "direction";
  enabled.dzn_meta.parent = &dzn_meta;
  enabled.dzn_meta.name = "enabled";


  connect(dutyAdd.api, impl.buttonDutyAdd);
  connect(dutySub.api, impl.buttonDutySub);
  connect(direction.api, impl.switchDirection);
  connect(enabled.api, impl.switchEnabled);
  connect(periodTimer.api, impl.periodTimer);
  connect(dutyTimer.api, impl.dutyTimer);

  dzn::rank(api.meta.provides.meta, 0);

}

void TestMotorSystem::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void TestMotorSystem::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}

////////////////////////////////////////////////////////////////////////////////



//version: 2.8.1