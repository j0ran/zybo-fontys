#include "buttonled.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>



ButtonLed::ButtonLed(const dzn::locator& dzn_locator)
: dzn_meta{"","ButtonLed",0,0,{& button.meta,& led.meta},{},{[this]{api.check_bindings();},[this]{button.check_bindings();},[this]{led.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)
, state(::ButtonLed::State::Idle)

, api({{"api",this,&dzn_meta},{"",0,0}})

, button({{"",0,0},{"button",this,&dzn_meta}})
, led({{"",0,0},{"led",this,&dzn_meta}})


{
  dzn_rt.performs_flush(this) = true;

  api.in.start = [&](){return dzn::call_in(this,[=]{ return api_start();}, this->api.meta, "start");};
  api.in.stop = [&](){return dzn::call_in(this,[=]{ return api_stop();}, this->api.meta, "stop");};
  button.out.turnedOn = [&](){return dzn::call_out(this,[=]{ return button_turnedOn();}, this->button.meta, "turnedOn");};
  button.out.turnedOff = [&](){return dzn::call_out(this,[=]{ return button_turnedOff();}, this->button.meta, "turnedOff");};





}

void ButtonLed::api_start()
{
  if ((state == ::ButtonLed::State::Idle && true)) 
  {
    this->button.in.start();
    this->led.in.turnOff();
    state = ::ButtonLed::State::Starting;
  }
  else if ((!((state == ::ButtonLed::State::Idle && true)) && true)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void ButtonLed::api_stop()
{
  if ((state == ::ButtonLed::State::Idle && true)) ;
  else if ((state == ::ButtonLed::State::Starting && true)) 
  {
    this->button.in.stop();
    state = ::ButtonLed::State::Idle;
  }
  else if ((state == ::ButtonLed::State::Pressed && true)) 
  {
    this->button.in.stop();
    this->led.in.turnOff();
    state = ::ButtonLed::State::Idle;
  }
  else if ((state == ::ButtonLed::State::Released && true)) 
  {
    this->button.in.stop();
    state = ::ButtonLed::State::Idle;
  }
  else if ((!((state == ::ButtonLed::State::Released && true)) && (!((state == ::ButtonLed::State::Pressed && true)) && (!((state == ::ButtonLed::State::Starting && true)) && (!((state == ::ButtonLed::State::Idle && true)) && true))))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void ButtonLed::button_turnedOn()
{
  if ((state == ::ButtonLed::State::Starting && true)) 
  {
    this->led.in.turnOn();
    state = ::ButtonLed::State::Pressed;
  }
  else if ((state == ::ButtonLed::State::Released && true)) 
  {
    this->led.in.turnOn();
    state = ::ButtonLed::State::Pressed;
  }
  else if ((!((state == ::ButtonLed::State::Released && true)) && (!((state == ::ButtonLed::State::Starting && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void ButtonLed::button_turnedOff()
{
  if ((state == ::ButtonLed::State::Starting && true)) 
  {
    state = ::ButtonLed::State::Released;
  }
  else if ((state == ::ButtonLed::State::Pressed && true)) 
  {
    this->led.in.turnOff();
    state = ::ButtonLed::State::Released;
  }
  else if ((!((state == ::ButtonLed::State::Pressed && true)) && (!((state == ::ButtonLed::State::Starting && true)) && true))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}


void ButtonLed::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void ButtonLed::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}




//version: 2.8.1