#include "ledtest.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>


//SYSTEM

LedTestSystem::LedTestSystem(const dzn::locator& dzn_locator)
: dzn_meta{"","LedTestSystem",0,0,{& buttonGlue.meta,& ledGlue.meta},{& buttonLed.dzn_meta,& button.dzn_meta},{[this]{api.check_bindings();},[this]{buttonGlue.check_bindings();},[this]{ledGlue.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)


, buttonLed(dzn_locator)
, button(dzn_locator)

, api(buttonLed.api)
, buttonGlue(button.glue), ledGlue(buttonLed.led)
{


  buttonLed.dzn_meta.parent = &dzn_meta;
  buttonLed.dzn_meta.name = "buttonLed";
  button.dzn_meta.parent = &dzn_meta;
  button.dzn_meta.name = "button";


  connect(button.api, buttonLed.button);

  dzn::rank(api.meta.provides.meta, 0);

}

void LedTestSystem::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void LedTestSystem::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}

////////////////////////////////////////////////////////////////////////////////



//version: 2.8.1