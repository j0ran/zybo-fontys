#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>

/********************************** INTERFACE *********************************/
#ifndef ITESTMOTOR_HH
#define ITESTMOTOR_HH




struct ITestMotor
{
#ifndef ENUM_ITestMotor_State
#define ENUM_ITestMotor_State 1


  struct State
  {
    enum type
    {
      Uninitialized,Idle,Operational
    };
  };


#endif // ENUM_ITestMotor_State

  struct
  {
    std::function< void()> initialize;
    std::function< void()> terminate;
    std::function< void()> start;
    std::function< void()> stop;
  } in;

  struct
  {
  } out;

  dzn::port::meta meta;
  inline ITestMotor(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.initialize) throw dzn::binding_error(meta, "in.initialize");
    if (! in.terminate) throw dzn::binding_error(meta, "in.terminate");
    if (! in.start) throw dzn::binding_error(meta, "in.start");
    if (! in.stop) throw dzn::binding_error(meta, "in.stop");


  }
};

inline void connect (ITestMotor& provided, ITestMotor& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}


#ifndef ENUM_TO_STRING_ITestMotor_State
#define ENUM_TO_STRING_ITestMotor_State 1
inline std::string to_string(::ITestMotor::State::type v)
{
  switch(v)
  {
    case ::ITestMotor::State::Uninitialized: return "State_Uninitialized";
    case ::ITestMotor::State::Idle: return "State_Idle";
    case ::ITestMotor::State::Operational: return "State_Operational";

  }
  return "";
}
#endif // ENUM_TO_STRING_ITestMotor_State

#ifndef STRING_TO_ENUM_ITestMotor_State
#define STRING_TO_ENUM_ITestMotor_State 1
inline ::ITestMotor::State::type to_ITestMotor_State(std::string s)
{
  static std::map<std::string, ::ITestMotor::State::type> m = {
    {"State_Uninitialized", ::ITestMotor::State::Uninitialized},
    {"State_Idle", ::ITestMotor::State::Idle},
    {"State_Operational", ::ITestMotor::State::Operational},
  };
  return m.at(s);
}
#endif // STRING_TO_ENUM_ITestMotor_State


#endif // ITESTMOTOR_HH

/********************************** INTERFACE *********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_BUTTONGLUE_HH
#define SKEL_BUTTONGLUE_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "zybo.hh"



namespace skel {
  struct ButtonGlue
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IButtonGlue api;


    ButtonGlue(const dzn::locator& dzn_locator)
    : dzn_meta{"","ButtonGlue",0,0,{},{},{[this]{api.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , api({{"api",this,&dzn_meta},{"",0,0}})


    {

      api.in.get = [&](){return dzn::call_in(this,[=]{ return api_get();}, this->api.meta, "get");};

    }
    virtual ~ ButtonGlue() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    friend std::ostream& operator << (std::ostream& os, const ButtonGlue& m)  {
      return m.stream_members(os);
    }
    private:
    virtual ::IButtonGlue::State::type api_get () = 0;

  };
}

#endif // BUTTONGLUE_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_LEDGLUE_HH
#define SKEL_LEDGLUE_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "zybo.hh"



namespace skel {
  struct LedGlue
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::ILedGlue api;


    LedGlue(const dzn::locator& dzn_locator)
    : dzn_meta{"","LedGlue",0,0,{},{},{[this]{api.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , api({{"api",this,&dzn_meta},{"",0,0}})


    {
      api.in.turnOn = [&](){return dzn::call_in(this,[=]{ return api_turnOn();}, this->api.meta, "turnOn");};
      api.in.turnOff = [&](){return dzn::call_in(this,[=]{ return api_turnOff();}, this->api.meta, "turnOff");};


    }
    virtual ~ LedGlue() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    friend std::ostream& operator << (std::ostream& os, const LedGlue& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void api_turnOn () = 0;
    virtual void api_turnOff () = 0;

  };
}

#endif // LEDGLUE_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_MOTORGLUE_HH
#define SKEL_MOTORGLUE_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "zybo.hh"



namespace skel {
  struct MotorGlue
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IMotorGlue api;


    MotorGlue(const dzn::locator& dzn_locator)
    : dzn_meta{"","MotorGlue",0,0,{},{},{[this]{api.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , api({{"api",this,&dzn_meta},{"",0,0}})


    {
      api.in.period = [&](int value){return dzn::call_in(this,[=]{ return api_period(value);}, this->api.meta, "period");};
      api.in.duty = [&](int value){return dzn::call_in(this,[=]{ return api_duty(value);}, this->api.meta, "duty");};
      api.in.cw = [&](){return dzn::call_in(this,[=]{ return api_cw();}, this->api.meta, "cw");};
      api.in.ccw = [&](){return dzn::call_in(this,[=]{ return api_ccw();}, this->api.meta, "ccw");};
      api.in.stop = [&](){return dzn::call_in(this,[=]{ return api_stop();}, this->api.meta, "stop");};


    }
    virtual ~ MotorGlue() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    friend std::ostream& operator << (std::ostream& os, const MotorGlue& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void api_period (int value) = 0;
    virtual void api_duty (int value) = 0;
    virtual void api_cw () = 0;
    virtual void api_ccw () = 0;
    virtual void api_stop () = 0;

  };
}

#endif // MOTORGLUE_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_ENCODERGLUE_HH
#define SKEL_ENCODERGLUE_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "zybo.hh"



namespace skel {
  struct EncoderGlue
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IEncoderGlue api;


    EncoderGlue(const dzn::locator& dzn_locator)
    : dzn_meta{"","EncoderGlue",0,0,{},{},{[this]{api.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , api({{"api",this,&dzn_meta},{"",0,0}})


    {
      api.in.position = [&](int& position){return dzn::call_in(this,[=, & position]{ return api_position(position);}, this->api.meta, "position");};

      api.in.direction = [&](){return dzn::call_in(this,[=]{ return api_direction();}, this->api.meta, "direction");};

    }
    virtual ~ EncoderGlue() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    friend std::ostream& operator << (std::ostream& os, const EncoderGlue& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void api_position (int& position) = 0;
    virtual ::IEncoderGlue::Dir::type api_direction () = 0;

  };
}

#endif // ENCODERGLUE_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_TIMER_HH
#define SKEL_TIMER_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "zybo.hh"



namespace skel {
  struct Timer
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::ITimer api;


    Timer(const dzn::locator& dzn_locator)
    : dzn_meta{"","Timer",0,0,{},{},{[this]{api.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , api({{"api",this,&dzn_meta},{"",0,0}})


    {
      api.in.create = [&](int miliseconds){return dzn::call_in(this,[=]{ return api_create(miliseconds);}, this->api.meta, "create");};
      api.in.cancel = [&](){return dzn::call_in(this,[=]{ return api_cancel();}, this->api.meta, "cancel");};


    }
    virtual ~ Timer() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    friend std::ostream& operator << (std::ostream& os, const Timer& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void api_create (int miliseconds) = 0;
    virtual void api_cancel () = 0;

  };
}

#endif // TIMER_HH

/***********************************  FOREIGN  **********************************/
/********************************** COMPONENT *********************************/
#ifndef TESTMOTOR_HH
#define TESTMOTOR_HH

#include "button.hh"
#include "button.hh"
#include "button.hh"
#include "button.hh"
#include "zybo.hh"
#include "zybo.hh"
#include "zybo.hh"



struct TestMotor
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;
#ifndef ENUM_TestMotor_State
#define ENUM_TestMotor_State 1


  struct State
  {
    enum type
    {
      Uninitialized,Idle,Starting1,Starting2,Operational
    };
  };


#endif // ENUM_TestMotor_State
#ifndef ENUM_TestMotor_ValueState
#define ENUM_TestMotor_ValueState 1


  struct ValueState
  {
    enum type
    {
      Idle,Increasing,Decreasing
    };
  };


#endif // ENUM_TestMotor_ValueState
#ifndef ENUM_TestMotor_DirectionState
#define ENUM_TestMotor_DirectionState 1


  struct DirectionState
  {
    enum type
    {
      StoppedCW,StoppedCCW,CW,CCW
    };
  };


#endif // ENUM_TestMotor_DirectionState

  ::TestMotor::State::type state;
  ::TestMotor::ValueState::type dutyState;
  int duty;
  int step;
  ::TestMotor::DirectionState::type directionState;


  std::function<void ()> out_api;

  ::ITestMotor api;

  ::IButton buttonDutyAdd;
  ::IButton buttonDutySub;
  ::IButton switchDirection;
  ::IButton switchEnabled;
  ::IMotorGlue motor;
  ::ITimer periodTimer;
  ::ITimer dutyTimer;


  TestMotor(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os) const;
  friend std::ostream& operator << (std::ostream& os, const TestMotor& m)  {
    (void)m;
    return os << "[" << m.state <<", " << m.dutyState <<", " << m.duty <<", " << m.step <<", " << m.directionState <<"]" ;
  }
  private:
  void api_initialize();
  void api_terminate();
  void api_start();
  void api_stop();
  void buttonDutyAdd_turnedOn();
  void buttonDutyAdd_turnedOff();
  void buttonDutySub_turnedOn();
  void buttonDutySub_turnedOff();
  void switchDirection_turnedOn();
  void switchDirection_turnedOff();
  void switchEnabled_turnedOn();
  void switchEnabled_turnedOff();
  void periodTimer_timeout();
  void dutyTimer_timeout();

  void initialize ();
};

#endif // TESTMOTOR_HH

/********************************** COMPONENT *********************************/
/***********************************  SYSTEM  ***********************************/
#ifndef TESTMOTORSYSTEM_HH
#define TESTMOTORSYSTEM_HH


#include <dzn/locator.hh>

#include "Timer.hh"
#include "Timer.hh"
#include "button.hh"
#include "button.hh"
#include "button.hh"
#include "button.hh"



struct TestMotorSystem
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;


  ::TestMotor impl;
  ::Timer periodTimer;
  ::Timer dutyTimer;
  ::Button dutyAdd;
  ::Button dutySub;
  ::Button direction;
  ::Button enabled;

  ::ITestMotor& api;

  ::IButtonGlue& buttonDutyAdd;
  ::IButtonGlue& buttonDutySub;
  ::IButtonGlue& switchDirection;
  ::IButtonGlue& switchEnabled;
  ::IMotorGlue& motor;

  TestMotorSystem(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os=std::clog) const;
};

#endif // TESTMOTORSYSTEM_HH

/***********************************  SYSTEM  ***********************************/


//version: 2.8.1
