#include <iostream>
#include <thread>

#include <dzn/pump.hh>
#include <dzn/locator.hh>

#include "TimerID.h"
#include "LedGlue.hh"
#include "ButtonGlue.hh"
#include "MotorGlue.hh"
#include "motortest.hh"


int main()
{
	std::cout << "Zybo Example Program Dezyne Motor" << std::endl;

	dzn::locator locator;

	dzn::pump pump;
	locator.set(pump);

	dzn::runtime runtime;
	locator.set(runtime);

	TimerID timerID;
	locator.set(timerID);

	TestMotorSystem testMotorSystem(locator);
	ButtonGlue buttonAdd(locator, "/sys/class/gpio/gpio968");
	ButtonGlue buttonSub(locator, "/sys/class/gpio/gpio969");
	ButtonGlue buttonDir(locator, "/sys/class/gpio/gpio961");
	ButtonGlue buttonEnable(locator, "/sys/class/gpio/gpio960");
	MotorGlue motor(locator, "/sys/class/PWM/PWM0");
	connect(buttonAdd.api, testMotorSystem.buttonDutyAdd);
	connect(buttonSub.api, testMotorSystem.buttonDutySub);
	connect(buttonDir.api, testMotorSystem.switchDirection);
	connect(buttonEnable.api, testMotorSystem.switchEnabled);
	connect(motor.api, testMotorSystem.motor);
	testMotorSystem.check_bindings();

	dzn::shell(pump, testMotorSystem.api.in.initialize);
	dzn::shell(pump, testMotorSystem.api.in.start);
	std::this_thread::sleep_for(std::chrono::hours(1));
	dzn::shell(pump, testMotorSystem.api.in.stop);
	dzn::shell(pump, testMotorSystem.api.in.terminate);
}
