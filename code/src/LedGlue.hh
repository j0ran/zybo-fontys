#pragma once

#include "zybo.hh"

class LedGlue : public skel::LedGlue
{
public:
	LedGlue(dzn::locator& locator, const std::string& path);

private:
    void api_turnOn() override;
    void api_turnOff() override;

private:
	const std::string m_path;
};
