#pragma once

#include <dzn/locator.hh>

#include "zybo.hh"

class EncoderGlue : public skel::EncoderGlue
{
public:
	EncoderGlue(dzn::locator& locator, const std::string& path);

private:
    void api_position(int& position) override;
    IEncoderGlue::Dir::type api_direction() override;

private:
	const std::string m_path;
};
