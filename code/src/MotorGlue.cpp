#include <fstream>
#include "MotorGlue.hh"

MotorGlue::MotorGlue(dzn::locator& locator, const std::string& path) :
	skel::MotorGlue(locator),
	m_path(path)
{
}

void MotorGlue::api_period(int value)
{
	std::ofstream(m_path + "/PERIOD") << value;
}

void MotorGlue::api_duty(int value)
{
	std::ofstream(m_path + "/DUTY") << value;
}

void MotorGlue::api_cw()
{
	std::ofstream(m_path + "/ENABLE") << 2;
}

void MotorGlue::api_ccw()
{
	std::ofstream(m_path + "/ENABLE") << 3;
}

void MotorGlue::api_stop()
{
	std::ofstream(m_path + "/ENABLE") << 0;
}
