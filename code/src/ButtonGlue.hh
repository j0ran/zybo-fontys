#pragma once

#include "zybo.hh"

class ButtonGlue : public skel::ButtonGlue
{
public:
	ButtonGlue(dzn::locator& locator, const std::string& path);

private:
    IButtonGlue::State::type api_get() override;

private:
	const std::string m_path;
};
