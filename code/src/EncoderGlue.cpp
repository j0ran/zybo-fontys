#include <fstream>
#include "EncoderGlue.hh"

EncoderGlue::EncoderGlue(dzn::locator& locator, const std::string& path) :
	skel::EncoderGlue(locator),
	m_path(path)
{
}

void EncoderGlue::api_position(int& position)
{
	std::int32_t value = 0;

	std::ifstream(m_path + "/value") >> value;

	position = value & 0x7FFFFFFF;
}

IEncoderGlue::Dir::type EncoderGlue::api_direction()
{
	std::int32_t value = 0;

	std::ifstream(m_path + "/value") >> value;

	return (value & 0x80000000) != 0 ? (IEncoderGlue::Dir::CW) : (IEncoderGlue::Dir::CCW);
}
