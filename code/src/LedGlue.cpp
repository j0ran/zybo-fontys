#include "LedGlue.hh"
#include <fstream>

LedGlue::LedGlue(dzn::locator& locator, const std::string& path) :
	skel::LedGlue(locator),
	m_path(path)
{
}

void LedGlue::api_turnOn() {
	std::ofstream(m_path + "/value") << 1;
}

void LedGlue::api_turnOff() {
	std::ofstream(m_path + "/value") << 0;
}
