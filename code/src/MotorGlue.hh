#pragma once

#include <dzn/locator.hh>

#include "zybo.hh"

class MotorGlue : public skel::MotorGlue
{
public:
	MotorGlue(dzn::locator& locator, const std::string& path);

private:
	void api_period(int value) override;
	void api_duty(int value) override;
	void api_cw() override;
	void api_ccw() override;
	void api_stop() override;

private:
	const std::string m_path;
};
