#include "ButtonGlue.hh"
#include <fstream>

ButtonGlue::ButtonGlue(dzn::locator& locator, const std::string& path) :
	skel::ButtonGlue(locator),
	m_path(path)
{
}

IButtonGlue::State::type ButtonGlue::api_get() {
	int value = 0;

	std::ifstream(m_path + "/value") >> value;

	return value == 0 ? IButtonGlue::State::Off : IButtonGlue::State::On;
}
